﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    class DeluxeRoom: HotelRoom
    {
        private bool additionalbed;
        public bool Additionalbed
        {
            get { return additionalbed; }
            set { additionalbed = value; }
        }
        public DeluxeRoom()
        {

        }
        public DeluxeRoom(string rt, string rn, string bc, int dr, bool ia, int no) : base(rt, rn, bc, dr, ia, no)
        {
           
        }
        public override string ToString()
        {
            return base.ToString();//+ " "+additionalbed;
        }
        public override double CalculateCharges()
        {
            double totalamount = DailyRate;
            if (additionalbed == true)
            {
                totalamount += 25;
            }
            return totalamount;
        }
    }
}   
