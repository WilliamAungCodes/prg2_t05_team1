﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    class StandardRoom:HotelRoom
    {
        private bool requireWifi;

        public bool RequireWifi
        {
            get { return requireWifi; }
            set { requireWifi = value; }
        }

        private bool requireBreakfast;

        public bool RequireBreakfast
        {
            get { return requireBreakfast; }
            set { requireBreakfast = value; }
        }
            
        

        public StandardRoom()
        {

        }

        public StandardRoom(string rt, string rn, string bc, int dr, bool ia, int no) : base(rt, rn, bc, dr, ia, no)
        {
           
        }

        public override double CalculateCharges()
        {
            double totalamount = DailyRate;
            if(requireBreakfast == true)
            {
                totalamount += 15;
            }
            if(requireWifi == true)
            {
                totalamount += 20;
            }
            return totalamount;
        }
        public override string ToString()
        {
            return base.ToString();// + " " +requireWifi+ " "+requireBreakfast;
        }
    }
}
