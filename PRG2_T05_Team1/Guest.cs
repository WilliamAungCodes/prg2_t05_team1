﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    class Guest
    {
        private string name;
        private string ppNumber;
        private Stay hotelStay;
        private Membership membership;
        private bool isCheckedIn;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string PpNumber
        {
            get { return ppNumber; }
            set { ppNumber = value; }
        }
        public Stay HotelStay
        {
            get { return hotelStay; }
            set { hotelStay = value; }
        }
        public Membership Membership
        {
            get { return membership; }
            set { membership = value; }
        }
        public bool IsCheckedIn
        {
            get { return isCheckedIn; }
            set { isCheckedIn = value; }
        }
        public Guest()
        {

        }
        public Guest(string n,string p,Stay h, Membership m,bool i)
        {
            name = n;
            PpNumber = p;
            HotelStay = h;
            Membership = m;
            isCheckedIn = i;
        }
        public override string ToString()
        {
            return (name + "  " + ppNumber + hotelStay + "  " + isCheckedIn);
        }
    }
}
