﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PRG2_T05_Team1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool fix = true;
        Guest checkguest;
        int bedcount;
        int num = -1;
        //All of our lists we will use
        List<HotelRoom> roomList = new List<HotelRoom>();
        List<HotelRoom> tempList = new List<HotelRoom>();
        List<Guest> guestList = new List<Guest>();
        List<HotelRoom> hotelroomListleft = new List<HotelRoom>();
        List<Membership> memberList = new List<Membership>();

        public MainPage()
        {
            this.InitializeComponent();
            hotelData();
                
            
        }
        public void refresh()
        {
            //To refresh the roomLV constantly
            hotelroomListleft.Clear();
            foreach (HotelRoom i in roomList)
            {
                if (i.IsAvail == true)
                {
                    hotelroomListleft.Add(i);
                }
            }
            
        }
        public void hotelData()
        {
            
            HotelRoom room1 = new StandardRoom("Standard", "101", "Single", 90,false,1);
            room1.NoOfOccupants = 1;
            StandardRoom r1= (StandardRoom)room1;
            r1.RequireBreakfast = true;
            r1.RequireWifi = true;
            HotelRoom room2 = new StandardRoom("Standard", "102", "Single", 90, true, 1);
          
            HotelRoom room3 = new StandardRoom("Standard", "201", "Twin", 110, true, 2);
            HotelRoom room4 = new StandardRoom("Standard", "202", "Twin", 110, false, 2);
            StandardRoom r4 = (StandardRoom)room4;
            r4.RequireBreakfast = true;
           
            room4.NoOfOccupants = 2;
            HotelRoom room5 = new StandardRoom("Standard", "203", "Twin", 110, true, 2);
            HotelRoom room6 = new StandardRoom("Standard", "301", "Triple  ", 120, true, 3);
            HotelRoom room7 = new StandardRoom("Standard", "302", "Triple", 120, false, 3);
            StandardRoom r7 = (StandardRoom)room7;
            r7.RequireBreakfast = true;
            room7.NoOfOccupants = 3;

            HotelRoom room8 = new DeluxeRoom("Deluxe", "204", "Twin", 140, true, 2);
            HotelRoom room9 = new DeluxeRoom("Deluxe", "205", "Twin", 140, true, 2);


            HotelRoom room10 = new DeluxeRoom("Deluxe", "303", "Triple", 210, false, 4);
            DeluxeRoom r10 = (DeluxeRoom)room10;
            r10.Additionalbed = true;
            room10.NoOfOccupants = 4;
            
            HotelRoom room11 = new DeluxeRoom("Deluxe", "304", "Triple", 210, true, 3);


            Stay stay1 = new Stay(DateTime.Parse("26/01/2019"), DateTime.Parse("02/02/2019"));
            Stay stay2 = new Stay(DateTime.Parse("25/01/2019"), DateTime.Parse("31/01/2019"));
            Stay stay3 = new Stay(DateTime.Parse("01/02/2019"), DateTime.Parse("6/02/2019"));
            Stay stay4 = new Stay(DateTime.Parse("28/01/2019"), DateTime.Parse("10/02/2019"));

          
            
            Membership membership1 = new Membership("Gold", 280);
            Membership membership2 = new Membership("Oridinary", 0);
            Membership membership3 = new Membership("Silver", 190);
            Membership membership4 = new Membership("Gold", 10);

            Guest guest1 = new Guest("Amelia", "S1234567A", stay1, membership1, true);
            
            Guest guest2 = new Guest("Bob", "G1234567A", stay2, membership2, true);
            Guest guest3 = new Guest("Cody", "G2345678A", stay3, membership3, true);
            Guest guest4 = new Guest("Edda", "S3456789A", stay4, membership4, true);


            memberList.Add(membership1);
            memberList.Add(membership2);
            memberList.Add(membership3);
            memberList.Add(membership4);


            guestList.Add(guest1);
            guestList.Add(guest2);
            guestList.Add(guest3);
            guestList.Add(guest4);


            roomList.Add(room1);
            roomList.Add(room2);
            roomList.Add(room3);
            roomList.Add(room4);
            roomList.Add(room5);
            roomList.Add(room6);
            roomList.Add(room7);
            roomList.Add(room8);
            roomList.Add(room9);
            roomList.Add(room10);
            roomList.Add(room11);

            guest1.HotelStay.RoomList.Add(room1);
            guest2.HotelStay.RoomList.Add(room7);
            guest3.HotelStay.RoomList.Add(room4);
            guest4.HotelStay.RoomList.Add(room10);

        }
       
        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void roomavail_Btn_Click(object sender, RoutedEventArgs e)
        {
            //Checks if the room is avail and put the room inside the hotelroomListleft
            List<HotelRoom> hotelroomListleft = new List<HotelRoom>();
            fix = true;
            foreach (HotelRoom i in roomList)
            {
                if (i.IsAvail == true)
                {
                    hotelroomListleft.Add(i);
                }
            }
            roomLV.ItemsSource = null;
            roomLV.ItemsSource = hotelroomListleft;
        }

        private void breakfast_Add_Checked(object sender, RoutedEventArgs e)
        {

            //HotelRoom i = (HotelRoom)roomLV.SelectedItem;

            //if(i!=null && i.RoomType == "Standard")
            //{
                
            //    i.DailyRate+=20;
            //    check.Text = i.DailyRate.ToString();
            //}
            
        }
        private void breakfast_Add_UnChecked(object sender, RoutedEventArgs e)
        {
            //HotelRoom i = (HotelRoom)roomLV.SelectedItem;
            //if(i != null &&i.RoomType == "Standard" )
            //{
            //    i.DailyRate -= 20;
            //    check.Text = (i.DailyRate.ToString());
            //}
           
        }

        private void extend_Btn_Click(object sender, RoutedEventArgs e)
        {

            foreach (Guest g in guestList)
            {

                if (g.Name == name_guest.Text || g.PpNumber == pass_num.Text)
                {
                    //Extend the CheckOutDate by 1
                        Invoice_Txt.Text = g.Name + g.PpNumber;
                        g.HotelStay.CheckOutDate = g.HotelStay.CheckOutDate.AddDays(1);
                        Invoice_Txt.Text += g.HotelStay.ToString();


                }

            }
        }

        private void wifi_Add_Checked(object sender, RoutedEventArgs e)
        {
            //HotelRoom i = (HotelRoom)roomLV.SelectedItem;

            //if (i != null)
            //{
            //    if (i.RoomType == "Standard")
            //    {

            //        i.DailyRate += 10;
            //        check.Text = (i.DailyRate.ToString());
            //    }
            //}


        }
        private void wifi_Add_UnChecked(object sender, RoutedEventArgs e)
        {

            /*HotelRoom i = (HotelRoom)roomLV.SelectedItem;
            if (i != null && i.RoomType == "Standard")
            {
                i.DailyRate -= 10;
                check.Text = (i.DailyRate.ToString());
            }
            */
        }
        private void bed_Add_Checked(object sender, RoutedEventArgs e)
        {
            /*HotelRoom b = (HotelRoom)roomLV.SelectedItem;
            if (b != null && b.RoomType == "Deluxe")
            {
                b.DailyRate += 25;
                check.Text = (b.DailyRate.ToString());
            }
            */
        }

        private void bed_Add_UnChecked(object sender, RoutedEventArgs e)
        {
            /*HotelRoom b = (HotelRoom)roomLV.SelectedItem;
            if (b != null && b.RoomType == "Deluxe")
            {
                b.DailyRate -= 25;
                check.Text = (b.DailyRate.ToString());
            }
            */
        }
        private void addroom_Btn_Click(object sender, RoutedEventArgs e)
        {
           //Validation
            wifi_Add.IsEnabled = false;
            breakfast_Add.IsEnabled = false;
            bed_Add.IsEnabled = false;
            if (fix == false){
                return;
            }

            //If Number of adults == 0 an error will pop out
            if(adults_Txt.Text == "0")
            {
                status_Txt.Text = "Please enter a reasonable adult number";
                return;
            }

            //If number of child is not inputed it will reset to 0
            if (child_Txt.Text == "")
            {
                child_Txt.Text = "0";
            }

            //If adult is not inputed an error will pop out
            if (adults_Txt.Text == "" )
            {
                status_Txt.Text = "Please make sure you have inputed the number of children or number of adults";
                return;
            }

            //Check if the checkboxes are clicked and changes the data accordingly 
            HotelRoom i = (HotelRoom)roomLV.SelectedItem;
            if (i is StandardRoom)
            {
                
                StandardRoom sr = (StandardRoom)i;

                
                
                if (wifi_Add.IsChecked == true)
                {                    
                  
                    sr.RequireWifi = true;
                }   
                if (breakfast_Add.IsChecked == true)
                {                    
                    sr.RequireBreakfast = true;

                }
                
                //sr.NoOfOccipants = Convert.ToInt32(adults_Txt.Text) + Convert.ToInt32(child_Txt.Text);
            }
            if (i is DeluxeRoom)
            {
                DeluxeRoom dr = (DeluxeRoom)i;
                
                if (bed_Add.IsChecked == true)
                {                   
                    
                    dr.Additionalbed = true;
                }
                //dr.NoOfOccipants= Convert.ToInt32(adults_Txt.Text) + Convert.ToInt32(child_Txt.Text);
            }
           
            //If the Room selected is not null, it will add Room to TempList
            if (i != null)
            {
                tempList.Add(i);
                i.IsAvail = false;
                selectedroom_LV.ItemsSource = null;
                selectedroom_LV.ItemsSource = tempList;
                hotelroomListleft.Remove(i);
            }
            roomLV.ItemsSource = null;
            roomLV.ItemsSource = hotelroomListleft;
            refresh();
        }
        
        private void removeroom_Btn_Click(object sender, RoutedEventArgs e)
        {
            // If person doesn't select a room, display an error
            if (selectedroom_LV.SelectedItem == null)
            {
                status_Txt.Text = "Error, please make sure u have selected a room";
                return;
            }

            //The moment the room is deselected, it will become available again.
            HotelRoom selected =(HotelRoom)selectedroom_LV.SelectedItem;            
            selected.IsAvail = true;
            //selected.NoOfOccipants = 0;
            foreach(HotelRoom i in tempList.Reverse<HotelRoom>())
            {
                //It deselects the checkboxes and changes the data accordingly
                if(i is StandardRoom)
                {
                    StandardRoom sr = (StandardRoom)i;
                    if (sr.RequireBreakfast == true)
                    {
                        sr.RequireBreakfast = false;
                        
                    }
                    if (sr.RequireWifi == true)
                    {
                        sr.RequireWifi = false;
                       
                    }
                    
                }
                if(i is DeluxeRoom)
                {
                    DeluxeRoom dr = (DeluxeRoom)i;
                    if(dr.Additionalbed == true)
                    {
                        dr.Additionalbed = false;
                        
                    }
                }
               
            }
            
            
            tempList.Remove((HotelRoom)selectedroom_LV.SelectedItem);
            selectedroom_LV.ItemsSource = null;
            selectedroom_LV.ItemsSource = tempList;
            
            roomLV.ItemsSource = null;
            roomLV.ItemsSource = hotelroomListleft;
            refresh();
        }

         private void selectedroom_LV_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {

            
            //To prevent checkboxes to be clicked when selecting a room from selectedroomLV      
            wifi_Add.IsEnabled = false; 
            breakfast_Add.IsEnabled = false;
            bed_Add.IsEnabled = false;
                    
                
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        

        private void checkin_Btn(object sender, RoutedEventArgs e)
        {
           
            bedcount = 0;

            //Validates if user inputed the date
            if (checkin_Date.Date== null || checkout_Date.Date== null)
            {
                status_Txt.Text = "Please type in a date";
                return;
            }

            //Check if the user has selected a room
            if(tempList.Count==0)
            {
                status_Txt.Text = "Please select a room";
            }
            if (child_Txt.Text == "")
            {
                child_Txt.Text = "0";
            }
            if (adults_Txt.Text == "")
            {
                status_Txt.Text = "Please make sure you have inputed the number of children or number of adults";
                return;
            }

            //To check if the user types in a number inside the adults_Txt and child_Txt
            if (!int.TryParse(adults_Txt.Text, out num))
            {
                status_Txt.Text = "Please type in a number into the adult box";
                return;
            }
            if (!int.TryParse(child_Txt.Text, out num))
            {
                status_Txt.Text = "Please type in a number into the child box";
                return;
            }
            
            //To check if the user types in a number that is too big
            if (Convert.ToInt64(child_Txt.Text) > 50)
            {
                status_Txt.Text = "Please enter a reasonable child amount";
                return;
            }
            double child = Math.Ceiling(Convert.ToDouble(child_Txt.Text)/2);
            
            //Calculate the total space the adult and child would take
            int totalocc = (Convert.ToInt32(adults_Txt.Text) + Convert.ToInt32(child));

            //Creates a new Guest
            double datedif =(checkout_Date.Date.Value.DateTime - checkin_Date.Date.Value.DateTime).TotalDays;
            DateTime check1 = checkin_Date.Date.Value.DateTime;
            DateTime check2 = checkout_Date.Date.Value.DateTime;
            Stay newstay = new Stay(checkin_Date.Date.Value.DateTime, checkout_Date.Date.Value.DateTime);
            Membership newmember = new Membership("Ordinary", 0);
            Guest newguest = new Guest(name_guest.Text.ToString(), pass_num.Text.ToString(), newstay, newmember, false);

            //Check if the user inputed a name or passport number, else display an error
            if (name_guest.Text == "" || pass_num.Text =="")
            {
                status_Txt.Text = "Please make sure you have inputed in a name and a passport number";
                return;
            }

            //Check if the checkin and checkout days are valid, else display an error
            if(datedif < 0)
            {
                status_Txt.Text = "Please make sure your checkin date is more than your checkout date";
                return;
            }

            //Check the total number of spaces the hotel room has.
            foreach (HotelRoom i in tempList)
            {

                if (i is StandardRoom)
                {
                    StandardRoom sr = (StandardRoom)i;
                    bedcount += sr.NoOfOccupants;
                    //if (sr.BedConfiguration == "Single")
                    //{
                    //    bedcount += 1;
                    //}
                    //if (sr.BedConfiguration == "Double")
                    //{
                    //    bedcount += 2;
                    //}
                    //if (sr.BedConfiguration == "Triple")
                    //{
                    //    bedcount += 3;
                    //}

                }
                if (i is DeluxeRoom)
                {
                    DeluxeRoom dr = (DeluxeRoom)i;
                    bedcount += dr.NoOfOccupants;
                    if (dr.Additionalbed == true)
                    {
                        bedcount += 1;
                    }
                    //if (dr.BedConfiguration == "Double")
                    //{
                    //    if (dr.Additionalbed == true)
                    //    {
                    //        bedcount += 3;
                    //    }
                    //    else
                    //    {
                    //        bedcount += 2;
                    //    }
                    //}
                    //if (dr.BedConfiguration == "Triple")
                    //{
                    //    if (dr.Additionalbed == true)
                    //    {
                    //        bedcount += 4;
                    //    }
                    //    else
                    //    {
                    //        bedcount += 3;
                    //    }
                    //}
                }
            }
            

            //Check if the number of occupants exceed the hotel room, if so an error will show.
            if (bedcount < totalocc)
            {
                status_Txt.Text = "The number of occupants exceed the maximum permissible number of occupants in the room";
                return;
            }
            
            foreach (Guest c in guestList)
            {

                //Check if guest name and Pp number is equal to entered name and Pp number.
                if (c.Name == name_guest.Text&&c.PpNumber==pass_num.Text)
                {

                    //Check if guest is already checked in, if so display an error.
                    if (c.IsCheckedIn == true)
                    {
                        status_Txt.Text = "Guest is already checked in";
                        return;
                    }
                   
                    //Checks in the existing guest and only updates their roomList.
                    else
                    {
                        newguest.IsCheckedIn = true;
                        c.HotelStay.RoomList.Clear();
                        foreach (HotelRoom i in tempList)
                        {
                            c.HotelStay.AddRoom(i);
                            selectedroom_LV.ItemsSource = null;
                            selectedroom_LV.ItemsSource = tempList;
                            roomLV.ItemsSource = null;
                            roomLV.ItemsSource = hotelroomListleft;

                        }
                        c.IsCheckedIn = true;
                        tempList.Clear();
                        return;
                    }
                    
                }
                //points = Convert.ToInt32(datedif * newguest.HotelStay.CalculateTotal());
                
            }
             
            //Check in new guests
            if (newguest.IsCheckedIn == false && checkin_Date.Date.Value.DateTime != null && checkout_Date.Date.Value.DateTime != null)
            {

                
                guestList.Add(newguest);


                //Make the rooms that the guests checked in unavailable
                foreach (HotelRoom b in tempList)
                {
                    b.IsAvail = false;
                    newguest.HotelStay.AddRoom(b);

                }

                tempList.Clear();
                selectedroom_LV.ItemsSource = null;
                selectedroom_LV.ItemsSource = tempList;
                roomLV.ItemsSource = null;
                roomLV.ItemsSource = hotelroomListleft;
                newguest.IsCheckedIn = true;
            }

            
        }

        private void check_out(object sender, RoutedEventArgs e)
        {
            foreach(Guest i in guestList)
            {
                if (i.Name == name_guest.Text || i.PpNumber == pass_num.Text)
                {

                    //if guest is checked out run this
                    if (i.IsCheckedIn == false)
                    {
                        status_Txt.Text = "Error, Guest is already checked out";
                    }


                    double points = 0;
                    double spent = 0;
                    double result = 0;


                    //if guest is checked in run this
                    if (i.IsCheckedIn == true)
                    {


                        //Calculate the points the guest earned during this stay
                        spent = i.HotelStay.CalculateTotal();
                        points = spent / 10;

                        //Return the points earned to EarnPoints method in Membership class
                        i.Membership.EarnPoints(points);


                        //Minus points redeemed from Membership class IF redeemed points is not NULL
                        if (point_Txt.Text != "")
                        {

                            //Check if points is less than 0, if so display an error
                            if (Convert.ToInt32(point_Txt.Text) < 0)
                            { Invoice_Txt.Text = "Please enter a valid number"; }
                            else
                            {
                                //Check if  guest have enough points, else display an error
                                if (i.Membership.Points - (Convert.ToInt32(point_Txt.Text)) < 0)
                                {
                                    Invoice_Txt.Text = "You do not have enough points";
                                }

                                //Minus redeemed points from Membership.Points
                                if (i.Membership.Points - (Convert.ToInt32(point_Txt.Text)) >= 0)
                                {
                                    i.Membership.Points = i.Membership.Points - (Convert.ToInt32(point_Txt.Text));
                                }
                            }
                        }
                        //Displays details of stay and membership to Invoice_Txt

                        Invoice_Txt.Text = i.Membership.ToString();
                        Invoice_Txt.Text += i.HotelStay.ToString();

                        //Changes checked in status to false
                        i.IsCheckedIn = false;

                        //Check if the text is not empty, if so, run 
                        if (point_Txt.Text != "")
                        {
                            //Check if the points redeemed is more than 0, if so, run
                            if (Convert.ToInt32(point_Txt.Text) > 0)
                            {
                                result = spent - Convert.ToDouble(point_Txt.Text);
                                Invoice_Txt.Text += "\n" + "New offset amount = $" + result + "\n" + "Points redeemed: " + point_Txt.Text;
                            }
                        }
                        status_Txt.Text = "Check out successful";
                    }

                        //Resets the additional requirements
                        foreach (HotelRoom b in i.HotelStay.RoomList)
                        {
                            b.IsAvail = true;
                            if (b is StandardRoom)
                            {
                                StandardRoom sr = (StandardRoom)b;
                                if (sr.RequireBreakfast == true)
                                {
                                    sr.RequireBreakfast = false;

                                }
                                if (sr.RequireWifi == true)
                                {
                                    sr.RequireWifi = false;

                                }

                            }
                            if (b is DeluxeRoom)
                            {
                                DeluxeRoom dr = (DeluxeRoom)b;
                                if (dr.Additionalbed == true)
                                {
                                    dr.Additionalbed = false;

                                }
                            }

                        }

                    
                }   
            }
        }

        private void search_Btn(object sender, RoutedEventArgs e)
        {   
            int count = 0;
            fix = false;
            Invoice_Txt.Text = "";
            roomLV.ItemsSource = null;
            
            foreach (Guest g in guestList)
            {
                //To check if the guest has been checked out
                if (g.IsCheckedIn == false)
                {
                    status_Txt.Text = "Guest has already checked out";
                }

                //Check if guest name and Ppnumber matches with input details
                if (g.Name == name_guest.Text && g.PpNumber == pass_num.Text)
                {

                    
                    roomLV.ItemsSource = null;
                    roomLV.ItemsSource = g.HotelStay.RoomList;
                    Invoice_Txt.Text += g.Membership.ToString();
                    Invoice_Txt.Text += g.HotelStay.ToString();
                    return;
                }


                //If Guest name and Ppnumber both doesnt match, checks if guest name matches with input name
                else if (g.Name == name_guest.Text)
                {

                    count += 1;
                    checkguest = g;
                    

                }

                //If Guest name and Ppnumber both doesnt match, checks if guest Ppnumber matches with input Ppnumber
                else if (g.PpNumber == pass_num.Text )
                {
                    count += 1;
                    checkguest = g;
                   

                }                            
            }
            
            //To validate guest that only entered name or pp number
            if (count == 1)
            {
                Invoice_Txt.Text =checkguest.IsCheckedIn + checkguest.PpNumber;
                roomLV.ItemsSource = null;
                roomLV.ItemsSource = checkguest.HotelStay.RoomList;
                Invoice_Txt.Text += checkguest.Membership.ToString();

                Invoice_Txt.Text += checkguest.HotelStay.ToString();

            }

            //To validates 2 people that has the same name inside the guestList
            else if (count > 1)
            {
                status_Txt.Text = "Existing name or Passport found, please enter both your name and passport";
                return;

            }

            //if guest details doesn't match with input details, display an error
            else
            {
                roomLV.ItemsSource = null;

                status_Txt.Text = "Guest not found. Please try again.";
            }
        }

        private void roomLV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HotelRoom i = (HotelRoom)roomLV.SelectedItem;
            

            //If room is standard, bed_Add will be disabled AND if they click on another standard bed their checkbox selection will be reset.
            if (i != null && i.RoomType == "Standard" )
            {
                wifi_Add.IsChecked = false;
                breakfast_Add.IsChecked = false;
                wifi_Add.IsEnabled = true;
                breakfast_Add.IsEnabled = true;
                bed_Add.IsEnabled = false;
            }


            //If room is deluxe, their breakfast_Add and wifi_add would already be enabled and they would not be able to click it.
            if(i != null &&i.RoomType == "Deluxe" && i!=null)
            {
                bed_Add.IsChecked = false;
                bed_Add.IsEnabled = true;
                wifi_Add.IsEnabled = false;
                breakfast_Add.IsEnabled = false;
                wifi_Add.IsChecked = true;
                breakfast_Add.IsChecked = true;
            }
        }

        private void Point_Txt_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

       

        private void redeem_Btn_Click_1(object sender, RoutedEventArgs e)
        {

            double spent = 0;
            // if point_txt has no value inside
            if(point_Txt.Text == "")
            {
                status_Txt.Text = "Please input a value into the point text";
                return;
            }
            //if point_txt is not a integer number
            if (!int.TryParse(point_Txt.Text, out num))
            {
                status_Txt.Text = "Please type in a number into the point text";
                return;
            }
            foreach (Guest i in guestList)
            {
                //Checks if guest name and Pp number matches the input data
                if (i.Name == name_guest.Text || i.PpNumber == pass_num.Text)
                {
                    //if entered redeem points is less than 0, display an error
                    if (Convert.ToInt32(point_Txt.Text) < 0)
                    { Invoice_Txt.Text = "Please enter a valid number";
                        return;
                    }
                    
                    else
                    {
                        //Check if member is able to redeem points
                        if (i.Membership.RedeemPoints(Convert.ToInt32(point_Txt.Text)) == true)
                        {
                            foreach (HotelRoom h in i.HotelStay.RoomList)
                            {

                                //Run if room type is standard and status is gold or silver
                                if (h.RoomType == "Standard" && i.Membership.Status == "Gold" || i.Membership.Status == "Silver")
                                {
                                    Invoice_Txt.Text = i.Name + i.PpNumber;
                                    Invoice_Txt.Text += i.Membership.ToString();
                                    Invoice_Txt.Text += i.HotelStay.ToString();
                                    spent += i.HotelStay.CalculateTotal();
                                    spent = spent - Convert.ToDouble(point_Txt.Text);
                                    Invoice_Txt.Text += "\n" + "New offset amount = $" + spent;
                                }

                                //Run if room type is Deluxe and status is gold
                                if (h.RoomType == "Deluxe" && i.Membership.Status == "Gold")
                                {
                                    Invoice_Txt.Text = i.Name + i.PpNumber;
                                    Invoice_Txt.Text += i.Membership.ToString();
                                    Invoice_Txt.Text += i.HotelStay.ToString();
                                    spent += i.HotelStay.CalculateTotal();
                                    spent = spent - Convert.ToDouble(point_Txt.Text);
                                    Invoice_Txt.Text += "\n" + "New offset amount = $" + spent;
                                }

                                //Run if room type is deluxe and status is silver
                                if (h.RoomType == "Deluxe" && i.Membership.Status == "Silver")
                                {
                                    Invoice_Txt.Text = "Sorry your status must be Gold to be able to redeem for Deluxe room.";
                                }
                            }

                        }
                        //If member is unable to redeem points, display an error
                        if (i.Membership.RedeemPoints(Convert.ToInt32(point_Txt.Text)) == false)
                        {
                            Invoice_Txt.Text = "Not applicable";
                        }
                    }
                }
            }
        }
    }
}
