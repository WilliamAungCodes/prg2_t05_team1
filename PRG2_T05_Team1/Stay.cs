﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    class Stay
    {
        private List<HotelRoom> roomList = new List<HotelRoom>();
        private DateTime checkIndDate;
        private DateTime checkOutDate;
        public List<HotelRoom> RoomList
        {
            get { return roomList; }
            set { roomList = value; }
        }
        public DateTime CheckInDate
        {
            get { return checkIndDate; }
            set { checkIndDate = value; }
        }
        public DateTime CheckOutDate
        {
            get { return checkOutDate; }
            set { checkOutDate = value; }
        }
        public Stay()
        {
            
        }
        public Stay(DateTime ci,DateTime co)
        {
            checkIndDate = ci;
            checkOutDate = co;
        }
        public void AddRoom(HotelRoom h)
        {
            roomList.Add(h);
        }
        public double CalculateTotal()
        {
            double totalvalue = 0;
            double value = 0;
            foreach(HotelRoom i in roomList)
            {
                if (i is StandardRoom)
                {
                    StandardRoom sr = (StandardRoom)i;
                    totalvalue+= sr.CalculateCharges();
                   

                }

                if (i is DeluxeRoom)
                {

                    DeluxeRoom dr = (DeluxeRoom)i;
                    totalvalue += dr.CalculateCharges();
                   


                }
                value = totalvalue * (checkOutDate.Date - checkIndDate.Date).TotalDays;
            }
            return (value);
        }
        public override string ToString()
        {
            string output = "";
            double totalamount = 0;
            output += "\n" + "Number of days booked: " + (checkOutDate.Date - checkIndDate.Date).TotalDays;
            foreach (HotelRoom i in roomList)
            {
               
                if (i is StandardRoom)
                {
                    StandardRoom sr = (StandardRoom) i;
                    totalamount+= sr.CalculateCharges();
                    output +="\n"+"Room Number: " +sr.RoomNumber+"\n"+ "Require Wifi: " + sr.RequireWifi + "\t" + "\n" + "Require breakfast: "+sr.RequireBreakfast+"\n"+"Total price of room: $"+sr.CalculateCharges();
                   
                }

                if(i is DeluxeRoom)
                {
                    
                    DeluxeRoom dr = (DeluxeRoom)i;
                    totalamount += dr.CalculateCharges();
                    output+="\n"+ "RoomNumber: " + dr.RoomNumber+ "\n" + "Require Bed: " + dr.Additionalbed + "\n" + "Total price of room: $" + dr.CalculateCharges();
                  

                }
                
            }
            return output +"\n"+ "Total amount of all the rooms: $"+totalamount*(checkOutDate.Date - checkIndDate.Date).TotalDays;
        }
    }
}
