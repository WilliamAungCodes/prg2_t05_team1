﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    class Membership
    {
        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private int points;

        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        public Membership()
        { }

        public Membership(string s, int p)
        {
            status = s;
            points = p;
        }

        public void EarnPoints(double i)
        {
            int counts = 0;
            int countg = 0;
            i = i + points;

            if (i < 100 && counts == 0 && countg == 0)
            {
                status = "Ordinary";
            }
            if (i >= 100 && countg == 0)
            {
                counts += 1;
                status = "Silver";
            }
            if (i >= 200)
            {
                countg += 1;
                status = "Gold";
            }
            if(counts == 1 && countg== 0)
            {
                status = "Silver";
            }
            if(countg==1)
            {
                status = "Gold";
            }
            points = Convert.ToInt32(i);

           
        }

        public bool RedeemPoints(int i)
        {

            if(i<=points)
            {
                return true;
            }
            if (i < 0)
            {
                return false;
            }

            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "\n" + "Status= " + status + "\n" + "Points= " + points;
        }
    }
}
