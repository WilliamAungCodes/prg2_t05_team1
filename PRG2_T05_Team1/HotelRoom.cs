﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T05_Team1
{
    abstract class HotelRoom
    {
        private string roomType;
        private string roomNumber;
        private string bedConfiguration;
        private double dailyRate;
        private bool isAvail;
        private int noOfOccupants;

        public string RoomType
        {
            get { return roomType; }
            set { roomType = value; }
        }
        public string RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }
        public string BedConfiguration
        {
            get { return bedConfiguration; }
            set { bedConfiguration = value; }
        }
        public double DailyRate
        {
            get { return dailyRate; }
            set { dailyRate = value; }
        }
        public bool IsAvail
        {
            get { return isAvail; }
            set { isAvail = value; }
        }
        public int NoOfOccupants
        {
            get { return noOfOccupants; }
            set { noOfOccupants= value; }
        }
        public HotelRoom()
        {

        }
        abstract public double CalculateCharges();
        

        public HotelRoom(string rt,string rn,string bc, double dr, bool ia, int no)
        {
            roomType = rt;
            roomNumber = rn;
            bedConfiguration = bc;
            dailyRate = dr;
            isAvail = ia;
            noOfOccupants = no;
        }
        public override string ToString()
        {
            return ("RoomType:"+roomType +"\t"+ "RoomNumber:"+roomNumber +"\n"+ "BedConfiguration:"+bedConfiguration + "\t"+ "DailyRate:"+dailyRate +"\n" +"Availability:"+isAvail+"\t" + "Number of occupants: "+noOfOccupants+"\n");
        }
    }
}
